 /** Parse game data into a human readable format
  * Remember if you are using this object you need to use
  *  try/catch so that unknown properties don't break things.
  */
formatConquerClubGameData = {

    "game_type" : function(value) {
        var game_type = {
            "S" : "Standard",
            "C" : "Terminator",
            "A" : "Assassin",
            "P" : "Polymorphic", // not referenced in API - usable?
            "D" : "Doubles",
            "T" : "Triples",
            "Q" : "Quadruples"
        };
        return game_type[value];
    },

    "initial_troops" : function(value) {
        var initial_troops = {
            "E" : "Automatic",
            "M" : "Manual"
        };
        return initial_troops[value];
    },

    "play_order" : function(value) {
        var play_order = {
            "S": "Sequential",
            "F" : "Freestyle"
        };
        return play_order[value];
    },

    "bonus_cards" : function(value) {
        var bonus_cards = {
            "1" : "No Spoils",
            "2" : "Escalating",
            "3" : "Flat Rate",
            "4" : "Nuclear",
            "5" : "Zombie"  // not referenced in API - usable?
        };
        return bonus_cards[value];
    },

    "fortifications" : function(value) {
        var fortifications = {
            "C" : "Chained",
            "O" : "Adjacent",
            "M" : "Unlimited",
            "P" : "Parachute",
            "N" : "None"
        };
        return fortifications[value];
    },

    "war_fog" : function(value) {
        var war_fog = {
            "Y" : "Fog",
            "N" : ""
        };
        return war_fog[value];
    },

    "trench_warfare" : function(value) {
        var trench_warfare = {
            "Y" : "Trench",
            "N" : ""
        };
        return trench_warfare[value];
    },

    "round_limit" : function(value) {
        if(value == 0 || value == "0") {
            return "None";
        } else {
            return value + " Rounds";
        }
    },

    "speed_game" : function(value) {
        // Also known as round_length in the table header
        if(value == "N") {
            return "24 Hours";
        } else {
            return value + " Minute(s)";
        }
    }

    /**
     * Below are many/all the currently identified properties
     * If required, add your logic in and expose the function.
     */
    // "game_number" : function(value) {
    //     return value;
    // },

    // "game_state" : function(value) {
    //     return value;
    // },

    // "tournament" : function(value) {
    //     return value;
    // },

    // "private" : function(value) {
    //     return value;
    // },

    // "map" : function(value) {
    //     return value;
    // },

    // "round" : function(value) {
    //     return value;
    // },

    // "poly_slots" : function(value) {
    //     return value;
    // },

    // "time_remaining" : function(value) {
    //     return value;
    // },

    // "chat" : function(value) {
    //     return value;
    // },

    // "events" : function(value) {
    //     return value;
    // },
}
