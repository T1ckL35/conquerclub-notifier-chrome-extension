
var bgPage = chrome.extension.getBackgroundPage();

function log(message) {
    // console.log(message);
    bgPage.console.log(message);
}

/**
 * Reads the relevant part of the api xml and builds a display
 * based on the data
 */
function showGames(gamesData) {
    var gamesTemplate = Handlebars.templates['games_table'];
    // now render our handlebars template content
    $("#games").empty().append(gamesTemplate(gamesData));
    // otherwise show error: 'Failed loading games list';
}

$(document).ready(function(){
    var bgpage = chrome.extension.getBackgroundPage();

    /**
     * Grabs the config form data
     */
    function getConfigFormData() {
        return {
            'config_user': $('#config_user').val(),
            'config_eliminated': $('#config_eliminated').val(),
            'config_notifications': $('#config_notifications').is(':checked'),
            'config_interval': parseInt($('#config_interval').val()) || 0
        }
    }

    /**
    * fills in the config form with previously saved data
    */
    function populateConfigForm(configData) {
        $('#config_user').val(configData.config_user);
        $('#config_eliminated').val(configData.config_eliminated);
        $('#config_notifications').attr("checked", configData.config_notifications);
        $('#config_interval').val(configData.config_interval);
    }

    function clearConfigForm() {
        $('#config_user').val('');
        $('#config_eliminated').val('');
        $('#config_notifications').attr("checked", false);
        $('#config_interval').val('');
    }

    // custom function test
    function displayGamesTab() {
        $( "#tabs" ).tabs( "option", "active", 0 );
        bgpage.ccnotifier.startTimer();
    }

    // if the user selects to use desktop notifications then we have to 
    // add a permissions check to allow them. It can only happen directly from
    // a user interaction - it can't be forced programmatically
    $("#config_notifications").change(function() {
        if(this.checked) {
            // 0 = permission allowed
            if (webkitNotifications.checkPermission() != 0) {
                webkitNotifications.requestPermission();
            }
        }
    });

    $(document).on('click', "a.gameno, a.rank, a.rating", function() {
        event.preventDefault();
        bgpage.ccnotifier.openTab($(this).attr('href'));    
    });

    /**
    * IN USE:
    * Starts jquery ui tabs
    */
    $(function() {
        $( "#tabs" ).tabs({
            beforeLoad: function(event, ui) {
                ui.jqXHR.fail(function() {
                    ui.panel.html(
                    "Unable to load this tab." );
                });
            },

            /**
            * If we already have saved config data then start polling the api
            */
            create: function(event,ui) {
                var configData = bgpage.ccnotifier.getConfig();
                if(configData != null) {
                    $( "#tabs" ).tabs( "option", "active", 0 );
                    bgpage.ccnotifier.startTimer();
                } else {
                    $( "#tabs" ).tabs( "option", "active", 1 );
                }
            },

            beforeActivate: function(event,ui) {
                // log('before tab activate');
            },

            activate: function(event,ui) {
                var configData = bgpage.ccnotifier.getConfig();
                // if we have previously saved config form data
                if(configData != null) {
                    // display either of the tabs as required
                    if(ui.newTab.index() == 0) {
                        // display the games
                        bgpage.ccnotifier.startTimer();
                    }
                    if(ui.newTab.index() == 1) {
                        // populate the config form
                        populateConfigForm(configData);
                    }
                } else {
                    // only display the config tab
                    $( "#tabs" ).tabs( "option", "active", 1 );
                }

                // grabs the config form data and if we have usable data then start checking for games
                $('#config-start').on('click', function(event) {
                    event.preventDefault();
                    // get, check, and if valid, save the config formdata
                    bgpage.ccnotifier.checkFormData(getConfigFormData(), displayGamesTab);
                });

                // stop polling the api
                $('#config-stop').on('click', function(event) {
                    event.preventDefault();
                    bgpage.ccnotifier.clearTimer();
                });

                // clear the form and localstorage data and stop polling
                $('#config-clear').on('click', function(event) {
                    event.preventDefault();
                    clearConfigForm();
                    bgpage.ccnotifier.clearConfig();
                });
            }
        });
    });
});



