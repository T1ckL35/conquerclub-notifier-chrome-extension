(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['games_table'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n    <tr class=\"odd ";
  if (stack1 = helpers.game_style) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.game_style); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n    <td align=\"center\" style=\"vertical-align:top;\">\n        <span class=\"gameno\">";
  if (stack1 = helpers.game_number) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.game_number); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>\n        <a class=\"gameno\" href=\"http://www.conquerclub.com/game.php?game=";
  if (stack1 = helpers.game_number) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.game_number); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">Enter Game</a>\n    </td>\n    <td style=\"vertical-align:top;\">\n        ";
  if (stack1 = helpers.game_type) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.game_type); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "<!-- (S)tandard, (C)Terminator, (A)ssassin, (D)oubles, (T)riples or (Q)uadruples -->\n        <br>\n        ";
  if (stack1 = helpers.initial_troops) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.initial_troops); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "<!-- (E)Automatic or (M)anual -->\n        <br>\n        ";
  if (stack1 = helpers.play_order) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.play_order); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "<!-- (S)equential or (F)reestyle -->\n    </td>\n    <td style=\"vertical-align:top;\">\n        ";
  if (stack1 = helpers.map) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.map); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "<br>\n        <!-- todo - if a config is enabled then do another request to get the game map thumbnail image -->\n        <!--a href=\"http://maps.conquerclub.com/Fractured_China6.L.jpg\" rel=\"lightbox\" title=\"Fractured China,133184\">\n            <img style=\"background-image:url(http://maps.conquerclub.com/Fractured_China.thumb.png)\" src=\"http://static.conquerclub.com/map_normal.png\" width=\"50\" height=\"34\" class=\"thumbnail\" alt=\"Fractured China\" title=\"Fractured China\">\n        </a-->\n    </td>\n    <td style=\"vertical-align:top;\">\n        ";
  if (stack1 = helpers.bonus_cards) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.bonus_cards); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "<!-- (1)No Spoils, (2)Escalating, (3)Flat Rate or (4)Nuclear -->\n        <br>\n        ";
  if (stack1 = helpers.fortifications) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.fortifications); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "<!-- (C)hained, (O)Adjacent or (M)Unlimited -->\n        <br>\n        ";
  if (stack1 = helpers.war_fog) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.war_fog); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  if (stack1 = helpers.trench_warfare) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.trench_warfare); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\n    </td>\n    <td style=\"vertical-align:top;\">\n        ";
  if (stack1 = helpers.round_limit) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.round_limit); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "<!-- 0 (unlimited), 20, 50 or 100 -->\n        <br>\n        ";
  if (stack1 = helpers.speed_game) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.speed_game); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "<!-- round length - (N)o, 5, 4, 3, 2 or 1. If N then display '24 Hours'. --><br>\n        Round ";
  if (stack1 = helpers.round) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.round); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + " - ";
  if (stack1 = helpers.time_remaining) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.time_remaining); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "             <br>\n        <!--span class=\"additionalClock\">Today @ <b>XX:XX pm</b></span-->\n        <!-- additionalClock bit above not implemented in the api - might also be user specific for the time zone so may remove this -->\n    </td>\n    <td style=\"vertical-align:top;\">\n        <ul class=\"players\">\n            <!--\n                LI TAG\n                    if player is 'READY' then use class=status_green.\n                    if player is 'WAITING' then use class=status_red.\n                    if player is 'PLAYING' then use class=status_yellow.\n                    don't know what the other states are yet\n                RANK\n                    format is:\n                        class=\"rank <type> r<rank_number>\"\n                    where <type>. is (p=paid, f=free)\n                    and rank_number is (0 to 15+?)\n                    example:\n                        a paid user with a rank of Lieutenant is:\n                            class=\"rank p r9\"\n            -->\n            ";
  stack1 = helpers.each.call(depth0, (depth0 && depth0.players), {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n        </ul>\n    </td>\n</tr>\n";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n                <li class=\"";
  if (stack1 = helpers.user_status) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.user_status); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n                    <a class=\"rank ";
  if (stack1 = helpers.account_type) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.account_type); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + " r";
  if (stack1 = helpers.rank_number) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.rank_number); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" title=\"";
  if (stack1 = helpers.rank_text) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.rank_text); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  if (stack1 = helpers.username) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.username); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "'s Profile\" href=\"http://www.conquerclub.com/forum/memberlist.php?mode=viewprofile&amp;un=";
  if (stack1 = helpers.username) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.username); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (stack1 = helpers.username) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.username); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "</a>\n                    <a class=\"rating\" title=\"";
  if (stack1 = helpers.rank_text) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.rank_text); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + " ";
  if (stack1 = helpers.username) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.username); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "'s Ratings\" href=\"http://www.conquerclub.com/player.php?mode=ratings1&amp;username=";
  if (stack1 = helpers.username) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.username); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (stack1 = helpers.user_rating) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.user_rating); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "</a>\n                    <span>";
  if (stack1 = helpers.user_status_text) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = (depth0 && depth0.user_status_text); stack1 = typeof stack1 === functionType ? stack1.call(depth0, {hash:{},data:data}) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span>\n                </li>\n            ";
  return buffer;
  }

  buffer += "<table class=\"listing\">\n<tr>\n    <th>&nbsp;</th>\n    <th>Game Type<br>Initial Troops<br>Play Order</th>\n    <th>Map</th>\n    <th>Spoils<br>Reinforcements<br>Special Gameplay</th>\n    <th>Round Limit<br>Round Length<br>Round - Time Remaining</th>\n    <th>Players</th>\n</tr>\n";
  stack1 = helpers.each.call(depth0, (depth0 && depth0.games), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</table>";
  return buffer;
  });
})();