
This is a google chrome extension that reads in a user's game information and displays in a popup next to the extension button.

Setup:

Open the chrome extensions window (Settings -> Extensions) and click the Developer Mode checkbox at the top right. Now drag the downloaded .crx file into your extensions list. Once you have confirmed the permissions popup you will have a greyed out conquerclub icon in your browser.

Configuration / Usage:

Click the new greyed out conquerclub icon in your browser to get the CCNotifier configuration popup. Add your username and a duration time for checks (minimum = 1 minute) and click start.
The browser icon should now turn red to indicate that it is running and you should be shown the 'games' tab in the popup. This should be populated with all the games that you are currently in (including eliminated ones at the minute). The list will refresh based on your interval settings in the config tab. To stop the extension polling the conquerclub site click the stop button in the config popup. Note, if after clicking stop you click the games tab it will start checking the server again.
Finally clicking clear in the config tab will clear out your username and interval time so you can add it in again or add another user to check.

Tech:
This extension polls the conquerclub api (http://www.conquerclub.com/api.php) for the currently displayed information.
It uses handlebars js to generate precompiled templates for the games list popup layout. At a later date the whole thing will use them.

Todo:
- Make sure the browser icon turns to a tick icon when at least one game is ready to play.
- Remove reliance on a copy of conquerclub.com's css file.
- Add in the map thumbnail (and clickable big map).
- Add in each player's rank icon
- Add in each player's game state icon (waiting, ready, playign etc)
- Add in each player's rating value
- Basically mirror the games list you see when you log in to conquerclub.com
- Plug into chrome notifications so that a system message can be broadcast when a game is ready. Maybe look at hooking it into mail alerts or similar.
- Any other ideas people can think of...

