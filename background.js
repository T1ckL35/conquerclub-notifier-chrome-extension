

chrome.browserAction.onClicked.addListener(function(tab) {
    // console.log('onclicked');
});

chrome.windows.onCreated.addListener(function() {
    var configData = ccnotifier.getConfig();
    if(configData !== null) {
        ccnotifier.startTimer();
    }
})

var ccnotifier = {

    // # TODO: change the url call to use the api.php page. Note, this can be called
    // #  real-time too! See here for more details/options.
    // # http://www.conquerclub.com/api.php

    // # get active games for a username. Allows you to query in real-time and retrieve extra data
    // # http://www.conquerclub.com/api.php?mode=gamelist&gs=A&names=Y&p1un=simon.mann
    
    // # this extra query can be used to get the map images (thumb, small, large) for the specified map
    // # http://www.conquerclub.com/api.php?mode=maplist&mp=<map_name>     # can be comma separated names
    // # note, the image path is: http://maps.conquerclub.com/<image_type>
    
    // # this gets a single map (can be comma separated) data
    // # http://www.conquerclub.com/api.php?mode=gamelist&gn=13594854&names=Y

    // path = 'http://www.conquerclub.com/rss.php?username='
    // # test path# this gets stats on a single (or comma separated list of) user(s)
    // # http://www.conquerclub.com/api.php?mode=player&un=simon.mann

    // path = 'http://www.conquerclub.com/rss.php?user_id='
    'call_usercheck' : 'http://www.conquerclub.com/api.php?mode=player&un=',
    'call_gamecheck' : 'http://www.conquerclub.com/api.php?mode=gamelist&gs=A&names=Y&p1un=',
    // 'call_gamecheck' : 'http://localhost/api.php?', // testing
    'conquerclub_data' : null,
    'conquerclub_ready' : null,
    'requestTimer' : null,
    'formatGameData' : formatConquerClubGameData,
    'systemNotification' : null,

    /**
     * Generic ajax function used to call a url and then run a passed in callback function
     * TODO: consider passing in an object of data {}
     */
    'callServer' : function(options) {
//        var dfd = new $.Deferred();

        var jqxhr = $.ajax({
            'url': options.url,
            'datatype': 'xml'
        })
        .done(function(xml) {
            console.log('callServer.done()');
//            dfd.resolve(xml);
        })
        .fail(function() {
            console.log('callServer.fail()');
        })
        .always(function() {
            // console.log('callServer.always()');
        });

        // Set another completion function for the request above
        jqxhr.always(function() {
            // console.log('callServer.always()');
        });
//        return dfd.promise();
        return jqxhr;
    },

    /**
     * Used to validate config form data prior to saving
     */
    'checkFormData' : function(formdata, callback) {
        var _this = this,
            configData = _this.getConfig(),
            url = this.call_usercheck+formdata.config_user;

        if(formdata != null && formdata.config_user != '' && formdata.config_interval >= 1) {
            $.when(
                _this.callServer({
                    "url" : url
                })
            ).then(
                // success
                function(xml) {
                    if($(xml).find('player').length == 1) {
                        _this.clearTimer();
                        _this.saveConfig(formdata);
                        callback();
                    }
                },
                // fail
                function() {
                    console.log('connection problem? - unable to check if user is valid');
                }
            );
        }
    },

    /**
     * Grabs the saved config data and uses it to query the server for 
     * a list of games the specified user is in.
     * Caches the data locally so that when a timer is used, the games list
     * popup will show the last retrieved data set until it refreshes.
     */
    'loadGamesInPopup' : function() {
        var _this = this,
            configData = _this.getConfig(),
            url = _this.call_gamecheck+configData.config_user;

        $.when(
            _this.callServer({
                "url" : url
            })
        ).then(
            // success
            function(xml) {
                _this.cacheGameData($(xml).find('game'));
                _this.updatePopup();
            },
            // fail
            function() {
                console.log('connection problem? - could not get the games list');
            }
        );
    },

    /**
     * pushes game data to the popup so that it can be refreshed
     */
    "updatePopup" : function() {
        var gameData = this.buildGameData();
        var popups = chrome.extension.getViews({type: "popup"});
        if (popups.length != 0) {
            var popup = popups[0];
            popup.showGames(gameData);
        }
    },

    "buildGameData" : function() {
        var _this = this,
            games = this.getCachedGameData(),
            gamesData = {
                "games" : []
            },
            configData = _this.getConfig();
        _this.updateIcon('active');

        // manually parse each game into a json object for handlebars templating
        games.each(function(){
            var gameData = {}
            $(this).children().each(function(){
                switch(this.nodeName) {
                    case "players":
                        gameData['players'] = [];
                        gameData['game_style'] = '';
                        $(this).children().each(function(){
                            var statusClass = 'status_';
                            switch($(this).attr('state')) {
                                case 'Ready':
                                    statusClass += 'green';
                                    break;
                                case 'Playing':
                                    statusClass += 'yellow';
                                    break;
                                default:
                                    statusClass += 'red';
                                    break;
                            }
                            // TODO: Add flag to this game so that it's background color is different
                            var state = $(this).attr('state');
                            if($(this).text() == configData.config_user && (state == 'Ready' || state == 'Playing')) {
                                gameData['game_ready'] = true;
                                gameData['game_style'] = 'game-ready';
                                statusClass += ' current-user';
                                // TODO: merge two lines below with config option to enable/disable system messages
                                _this.updateIcon('ready');
                                if(configData.config_notifications == true) {
                                    _this.gameReadyNotification();
                                }
                            }

                            var player = {
                                "username" : $(this).text(),
                                "user_status" : statusClass,
                                "user_status_text" : state
                            };
                            gameData.players.push(player);
                        });
                        break;

                    default:
                        try {
                            gameData[this.nodeName] = _this.formatGameData[this.nodeName]($(this).text());
                        } catch(err) {
                            // it just means we don't have a function to handle the property
                            gameData[this.nodeName] = $(this).text();
                        }
                        break;
                }
            });
            gamesData.games.push(gameData);
        });
        return gamesData;
    },

    /**
     * Grab the returned api xml and trawl it to see if the game is ready.
     * When done, save the game data to a cache for displaying when requested.
     */
    'cacheGameData' : function(gameslist) {
        this.conquerclub_data = gameslist;
    },

    /**
     * When recreating the games list popup we need to populate it with a cached
     * set of game data until the next timer call is run
     */
    "getCachedGameData" : function() {
        return this.conquerclub_data;
    },

    /**
     * Starts a timer to check the server api every n minutes.
     * If there is already a timer then it doesn't do anything.
     * Note, we will have to recreate the timer if the interval
     *  value changes in the config.
     */
    'startTimer' : function() {
        var _this = this,
            configData = _this.getConfig()
            intervalMultipler = 60000;
        // NOTE, the intervalMultipler is currently in seconds for testing
        // so 10 will equal 10 seconds
        // When ready for live, change it to 60000 for minutes

        _this.updateIcon('active');
        if(_this.requestTimer == null) { // TODO: SORT THIS BIT || configData.config_interval != interval) {
            _this.loadGamesInPopup();
            _this.requestTimer = setInterval(function () {
                _this.loadGamesInPopup();
            }, (configData.config_interval * intervalMultipler));
        } else {
            _this.updatePopup();
        }
    },

    /**
     * Destroys a timer if one has previously been used
     */
    'clearTimer' : function() {
        this.updateIcon('inactive');
        clearInterval(this.requestTimer);
        this.requestTimer = null;
    },

    /**
     * saves/updates the config form data to localstorage and starts a timer to poll the api
     */
    "saveConfig" : function(formdata) {
        configData = JSON.stringify(formdata);
        localStorage.config = configData;
    },

    /**
     * return cached config data or if that isn't available return the saved
     * localstorage config data
     * If successful then the user must be a valid one.
     */
    "getConfig": function() {
      var rawConfig = localStorage.getItem("config");
      if (rawConfig !== null) {
          return JSON.parse(rawConfig);
      }
      return null;
    },

    /**
     * Clear the user's data from localstorage, clear the timer
     * and set the icon to inactive
     */
    "clearConfig": function() {
        this.clearTimer();
        this.updateIcon('inactive');
        localStorage.removeItem('config');
    },

    /**
     * Update the browser extension icon.
     * If we have a game ready then change it to a tick, otherwise use the default icon.
     */
    "updateIcon" : function(iconstatus) {
        var iconList = {
            'inactive' : '/images/cc_inactive.png',
            'active' : '/images/cc.png',
            'ready' : '/images/cc2.png',
        }
        chrome.browserAction.setIcon({
            'path' : iconList[iconstatus]
        });
    },

    /**
     * Chrome specific way of opening new tabs
     * Extracted to here so it is easier to handle porting to firefox/safari
     */
    "openTab" : function(url) {
        chrome.tabs.create({
            "url" : url
        });  
    },

    "gameReadyNotification" : function() {
        if(this.systemNotification != null) {
            this.systemNotification.cancel();
        }
        this.systemNotification = webkitNotifications.createNotification(
            '',  // icon url - can be relative
            'CC Notifier',  // notification title
            'Conquerclub game is ready!'  // notification body text
        );
        // Then show the notification.
        this.systemNotification.show();
    }

};
